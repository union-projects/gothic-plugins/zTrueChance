#include <Windows.h>
#include <random>


class zRandomizer {
  std::random_device Device;
  std::mt19937 Generator;
public:
  zRandomizer();
  int GetNext( int max );
};


zRandomizer::zRandomizer() : Device(), Generator(Device()) {
}


int zRandomizer::GetNext( int max ) {
  std::uniform_int_distribution<int> distribution( 0, max );
  return distribution( Generator );
}


class zTrueChance {
  zRandomizer Randomizer;
  float ControlRange;
  float Penalty;
  float TryCount;
  float FailCount;
public:

  zTrueChance();
  int GetNext( int chance );
};


zTrueChance::zTrueChance() {
  ControlRange = 25;
  Penalty = 0.02f;
  TryCount = 0;
  FailCount = 0;
}


int zTrueChance::GetNext( int chance ) {
  if( chance <= 0 )
    return 0;

  if( chance >= 100 )
    return 1;

  float chanceF = (float)chance;
  bool isChanceTooLow = chance < 10;
  int result = 0;

  float failCountMax = !isChanceTooLow ?
    (100.0f - chanceF) * 0.1f :
    (14.0f  - chanceF) * 2.0f;

  if( FailCount <= failCountMax ) {
    float randomValue = (float)Randomizer.GetNext(100);
    float totalChance = chanceF;

    if( !isChanceTooLow ) {
      float chanceMultiplier = TryCount / ControlRange;
      totalChance += 100.0f * chanceMultiplier;
    }

    result = randomValue <= totalChance;
  }
  else
    result = 1;

  if( result ) {
    TryCount = (-100.0f + chanceF) * Penalty;
    FailCount = 0;
  }
  else {
    TryCount++;
    FailCount++;
  }

  return result;
}