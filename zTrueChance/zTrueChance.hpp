
namespace GOTHIC_NAMESPACE {
  unordered_map<oCNpc*, zTrueChance*> TrueChances;
  zTrueChance& GetTrueChance( oCNpc* npc ) {
    if( TrueChances.find( npc ) == TrueChances.end() ) {
      auto trueChance = new zTrueChance();
      TrueChances[npc] = trueChance;
      return *trueChance;
    }

    return *TrueChances[npc];
  }


  void CheckForResetTrueChancesList() {
    static oCNpc* lastPlayer = player;
    if( lastPlayer != player ) {
      for( auto [k, v] : TrueChances )
        delete v;

      TrueChances.clear();
      lastPlayer = player;
    }
  }


  void __fastcall oCNpc_OnDamage_Hit_Random( Registers& reg );

#if ENGINE == Engine_G1
  auto Hook_oCNpc_OnDamage_Hit_Random = CreatePartialHook( (void*)0x007328B7, &oCNpc_OnDamage_Hit_Random);
#elif ENGINE == Engine_G2A
  auto Hook_oCNpc_OnDamage_Hit_Random = CreatePartialHook( (void*)0x00668695, &oCNpc_OnDamage_Hit_Random);
#endif

  void __fastcall oCNpc_OnDamage_Hit_Random( Registers& reg ) {
    CheckForResetTrueChancesList();

#if ENGINE == Engine_G1
    auto damageDesctiptor = *(oCNpc::oSDamageDescriptor**)(reg.esp + 0x228);
    auto npc = damageDesctiptor->pNpcAttacker;
    auto& chance = reg.ebp;
    auto& value = reg.edi;
#elif ENGINE == Engine_G2A
    auto damageDesctiptor = *(oCNpc::oSDamageDescriptor**)(reg.esp + 0x288);
    auto npc = damageDesctiptor->pNpcAttacker;
    auto& chance = reg.edi;
    auto& value = reg.ebp;
#endif

    auto critical = GetTrueChance( npc ).GetNext( chance );
    value = critical ?
      chance - 1 :
      chance + 1;
  }


  void __fastcall oCAIArrow_ReportCollisionToAI_Random( Registers& reg );

#if ENGINE == Engine_G1
  auto Hook_oCAIArrow_ReportCollisionToAI_Random = CreatePartialHook( (void*)0x0061977A, &oCAIArrow_ReportCollisionToAI_Random );
#elif ENGINE == Engine_G2A
  auto Hook_oCAIArrow_ReportCollisionToAI_Random = CreatePartialHook( (void*)0x006A1A05, &oCAIArrow_ReportCollisionToAI_Random );
#endif

  void __fastcall oCAIArrow_ReportCollisionToAI_Random( Registers& reg ) {
#if ENGINE == Engine_G1
    auto& chance = *(float*)(reg.esp + 0x14);
    auto* npc = (oCNpc*)reg.edi;
    auto& value = reg.edx;
#elif ENGINE == Engine_G2A
    auto& chance = *(int*)(reg.esp + 0x18);
    auto* npc = *(oCNpc**)(reg.ebp + 0x5C);
    auto& value = reg.edx;
#endif
    
    auto registered = GetTrueChance( npc ).GetNext( chance );
    value = registered ?
      chance - 1 :
      chance + 1;
    
#if ENGINE == Engine_G1
    reg.eip = 0x00619787;
#elif ENGINE == Engine_G2A
    reg.eip = 0x006A1A12;
#endif
  }


  int Hlp_Random();

#if ENGINE == Engine_G1
  auto Hook_Hlp_Random = CreateHook( (void*)0x0065D1F0, &Hlp_Random );
#elif ENGINE == Engine_G2A
  auto Hook_Hlp_Random = CreateHook( (void*)0x006F7810, &Hlp_Random );
#endif

  int Hlp_Random() {
    auto par = zCParser::GetParser();
    int interval;
    par->GetParameter( interval );

    static zRandomizer randomizer;
    int result = randomizer.GetNext( interval - 1 );
    par->SetReturn( result );
    return 0;
  }
}
