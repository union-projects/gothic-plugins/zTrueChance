﻿#include <Union/String.h>
#include <Union/Locale.h>
#include <Union/Hook.h>
using namespace Union;

#include "zTrueChance.h"

int main() {
  zTrueChance tc;

  srand( GetTickCount() );

  for( int p = 0; p <= 100; p++ ) {
    String::Format( "chance %i: \t", p ).StdPrint();

    int fails = 0;
    int failsMax = 0;
    int hits = 0;
    int imax = 10000;

    for( int i = 0; i < imax; i++ ) {
      int value = tc.GetNext( p );
      if( value ) {
        if( failsMax < fails )
          failsMax = fails;

        fails = 0;
        hits++;
      }
      else
        fails++;

      if( i < 100 )
        StringUTF16( value != 0 ? L'■' : L'.' ).StdPrint();
    }

    String::Format( "\tmax failed sequence:%i\ttotal crits:%i (%f%%)", failsMax, hits, 100.0f / (float)imax * (float)hits ).StdPrintLine();
  }
}

